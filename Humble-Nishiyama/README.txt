Humble Nishiyama Game

This is a game where both players think they have an equal opportunity to win.
The game has picked up in popularity in the Data Science community as there is
a technique that can help a player win the majority of the plays. You can read up
more about it here:
https://www.futilitycloset.com/2016/12/31/humble-nishiyama-randomness-game/

To play the game, you pick 3 cards. They can be either Black or Red.
Enter it into the game by typing them in. e.g. 'BRB' without the quotes.

The computer will then generate a set of card. First card to match the generated cards
with their picked cards, wins the game. 

Good luck!