//
//  main.cpp
//  Hello World
//
//  Created by Danilo Braga on 10/18/19.
//  Copyright © 2019 Danilo Braga. All rights reserved.
//

#include <iostream>
#include <ctime>
using namespace std;


int main()
{
    
    char playerCard[3], computerCard[3], generated, current[3] = {'c', 'c', 'c'};
    int random;
    bool endOfGame = false;

    //explain game rules
    cout << "Humble Nishiyama" << endl << endl;
    cout << "Game rules: You pick a three-card sequance of blacks and reds.\n";
    cout << "The goal is to guess what card sequence will come up as random cards are drawn." << endl;
    cout << "Lets play! Enter 'B' for Black and 'R' for Red." << endl << endl;

    srand(time(0));

    for (int i = 0; i < 3 ; i++) //generates computer's initial cards
    {
        for (int i = 0; i < 3; i++)
        {
            if (rand() %2 == 0)
            {
                computerCard[i] = 'B';
            }
            else
                computerCard[i] = 'R';
        }
    }
    cout << "The computer's selection is: " << computerCard[0] << computerCard[1] << computerCard[2] << endl;
    
    
    //get player's cards
    

    cout << "Enter your card selection: ";
    cin >> playerCard[0] >> playerCard[1] >> playerCard[2];
    
    
    //Generates the set of cards both players try to guess
    for (int i = 0; i < 3; i++)
    {
        if (rand() %2 == 0)
        {
            generated = 'B';
        }
        else
            generated = 'R';
        
        current[i] = generated;
    }
    cout << "The cards are: " << current[0] << current[1] << current[2] << endl;
    
    
    while (!endOfGame)
    {
        if
            (
             playerCard[0] == current[0]
             && playerCard[1] == current[1]
             && playerCard[2] == current[2]
             
             && computerCard[0] == current[0]
             && computerCard[1] == current[1]
             && computerCard[2] == current[2]
             )
        {
            cout << endl << "Both players win!" << endl;
            endOfGame = true;
        }
        else if
            (
             playerCard[0] == current[0]
             && playerCard[1] == current[1]
             && playerCard[2] == current[2]
             )
        {
            cout << endl << "You win!" << endl;
            endOfGame = true;
        }
        else if
            (
             computerCard[0] == current[0]
             && computerCard[1] == current[1]
             && computerCard[2] == current[2]
             )
        {
            cout << endl << "Computer wins!" << endl;
            endOfGame = true;
        }
        else //Nobody wins this set. Generate new last card.
        {
            if (rand() %2 == 0)
                random = 'B';
            else
                random = 'R';
            
            cout << endl << "drawing another card..." << endl;
                current[0] = current[1];
                current[1] = current[2];
                current[2] = random;
            cout << "The new cards are: " << current[0] << current[1] << current[2] << endl;
            
        }
    }
    
    

    
  

    
    
    
    
    
    return 0;
}
