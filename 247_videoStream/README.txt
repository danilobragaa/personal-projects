I have full permission from my company to share these scripts.

The purpose of this project was to provide a way that company administrators could watch a construction live from their offices as it happened, without ever stepping foot outside.
I reached out to companies that specialize in construction documenting and I was quoted thousands in installation costs and then hundreds in monthly bills to operate their cameras. I came up with a cheaper alternative that could record and capture stills throughout the 2 year long construction.




This project was written on a Mac using bash and FFMPEG. If you are also on a Mac and choose to use my code, I recommend downloading Homebrew and then installing FFMPEG through it.
If you choose to copy and paste the code, make sure you fill in the input and output quotes in the bash scripts to their proper links. Do not include the opening and closing quotes.

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Then to install FFMPEG

brew install ffmpeg

This will also ensure you have the latest version of ffmpeg.





The three cameras I used for this project are Hikvision DS-2CD2685FWD-IZS 8MP cameras. They have the ability to output a highly compressed RTSP stream at 4K leaving little for the RTMP encoder to do.
These cameras have dual streams that can be accessed via:
"rtsp://user:password@192.168.1.15:554//Streaming/Channels/101"
And
"rtsp://user:password@192.168.1.15:554//Streaming/Channels/102"

Note that this requires a login to be entered through the link, as well as the IP address. The port stays the same unless changed in the configuration of the camera.


Next, I used a bash script [triggerCam#.command] that triggers the ffmpeg command script that streams to the RTMP server. For some reason, ffmpeg would run a very random amount of time anywhere from 30 minutes to 18 hours and then stop on its own. I resolved this inconsistency by having the bash script call itself. This would create a consistent stream to the RTMP server until the terminal was closed by an external command.
I will explain how I did this in the next paragraph.
Note that the bash scripts need to have their permissions changed. You can do this by navigating to the directory that it is in through Terminal, and then typing:

chmod a+x "file.command"

Do not include the quotes.
You must do this to all .command files.




Next I set up two Mac minis within the network that stream the feeds to the RTMP server with interruptions for archiving and the other Mac mini to capture a photo every 15 minutes and label it with the appropriate time and date.
I accomplished this by using Mac OS' built-in cron. The code for these can be found here in the repository. Make sure you add it to your cron tasks via terminal.
The cron tasks trigger the snapCam.command that captures stills,
it triggers the triggerCam.command that initiates ffmpeg to stream,
And every ~6 hours, it exits terminal for 5 minutes in order for the RTMP server to archive what was streamed.
NOTE: Check that your Mac mini is set to auto-boot up when power is restored. This makes the setup completely automated including through power outages.




That's it. The cameras stream 24/7 (mostly) with 3 interruptions per day to archive the streamed content. This has been useful when construction managers wanted to see footage from days prior.
You can check out all the code here in the repository.


SnapCam1.command This captures a still from Cam1
SnapCam2.command This captures a still from Cam2
triggerCam1.command this triggers ffmpeg to begin the livestream for Cam1
triggerCam2.command this triggers ffmpeg to begin the livestream for Cam2
triggerCam3.command this triggers ffmpeg to begin the livestream for Cam3
crontab.txt (MAC1) this is the crontab tasks for the livestream
crontab2.txt (MAC2) this is the crontab tasks for the stills


Known problems:
Because	Terminal is being forced quit as it runs ffmpeg, as soon as it opens back up, all prior windows open along with it. After enough windows build up, the stream will sometimes not start properly or refuse to start at all. 
The solution to this is to login to the computer once a week and close all Terminal windows except the one currently running ffmpeg.



Thank you for your interest in my project!
Danilo Braga
Video Engineer
