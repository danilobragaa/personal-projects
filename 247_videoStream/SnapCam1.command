#!/bin/bash

ffmpeg -rtsp_transport tcp -i "RTSP SRC LINK" -frames 1 -f image2 -strftime 1 /Users/admin/Documents/constructionStills/%Y-%m-%d_%H-%M-%S_constructionStill.jpg

exit

#this script takes stills from the RTSP stream and stores it locally.
#note that it also names the still photo based on the time it was taken.